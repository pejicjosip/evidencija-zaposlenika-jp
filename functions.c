#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>
#include "Header.h"




void dodajZaposlenikaUbazu()
{
	ZAPOSLENIKiNFO dodajInformacijeUbazu = { 0 };
	FILE* fp = NULL;
	int status = 0;
	fp = fopen(NAZIV_DATOTEKE, "ab+");
	if (fp == NULL)
	{
		printf("Datoteka nije otovrena\n");
		exit(1);
	}
	nazivOdabranogIzbornika("DODAJ NOVOG ZAPOSLENIKA");
	printf("\n\n\t\t\tUNESITE TRAZENE INFORMACIJE:");
	printf("\n\n");
	printf("\n\t\t\tZaposlenikov ID  = ");
	fflush(stdin); //sluzi za ciscenje ostataka u memoriji (buffera)
	scanf("%u", &dodajInformacijeUbazu.idZaposlenika);
	char ocisti[5];                                                         //spremnik za nezeljene znakove koji su "propadali" u varijablu ime oca
	fgets(ocisti, 4, stdin);
	do
	{
		printf("\n\t\t\tIme oca  = ");
		fflush(stdin);
		fgets(dodajInformacijeUbazu.imeOca, MAX_IME_OCA, stdin);
		status = provjeraImena(dodajInformacijeUbazu.imeOca);
		if (status == 0)
		{
			printf("\n\t\t\tUnos sadrzava nedozvoljene znakove. Pokusaj ponovno.");
		}
	} while (status == 0);
	do
	{
		printf("\n\t\t\tIme zaposlenika  = ");
		fflush(stdin);
		fgets(dodajInformacijeUbazu.imeZaposlenika, MAX_IME_ZAPOSLENIKA, stdin);
		status = provjeraImena(dodajInformacijeUbazu.imeZaposlenika);
		if (status == 0)
		{
			printf("\n\t\t\tUnos sadrzava nedozvoljene znakove. Pokusaj ponovno.");
		}
	} while (status == 0);

	do
	{
		printf("\n\t\t\tPrezime zaposlenika  = ");
		fflush(stdin);
		fgets(dodajInformacijeUbazu.prezime, MAX_PREZIME, stdin);
		status = provjeraImena(dodajInformacijeUbazu.prezime);
		if (status == 0)
		{
			printf("\n\t\t\tUnos sadrzava nedozvoljene znakove. Pokusaj ponovno.");
		}
	} while (status == 0);
	do
	{
		printf("\n\t\t\tAdresa zaposlenika = ");
		fflush(stdin);
		fgets(dodajInformacijeUbazu.adresaZaposlenika, MAX_ADRESA_ZAPOSLENIKA, stdin);
		status = provjeraAdrese(dodajInformacijeUbazu.adresaZaposlenika);
		if (status == 0)
		{
			printf("\n\t\t\tUnos sadrzava nedozvoljene znakove. Pokusaj ponovno.");
		}
	} while (status == 0);

	printf("\n\t\t\tPlaca zaposlenika  = ");
	fflush(stdin);
	scanf("%f", &dodajInformacijeUbazu.placaZaposlenika);
	do {
		printf("\n\t\t\tUnesite datum u formatu (dd.mm.gggg): ");
		scanf("%d.%d.%d", &dodajInformacijeUbazu.datumZaposljavanja.dd, &dodajInformacijeUbazu.datumZaposljavanja.mm, &dodajInformacijeUbazu.datumZaposljavanja.gggg);
		status = provjeriDatum(&dodajInformacijeUbazu.datumZaposljavanja);
	} while (status == 0);

	fwrite(&dodajInformacijeUbazu, sizeof(dodajInformacijeUbazu), 1, fp);        //pisanje u datoteku (zapisivanje jedne strukture)
	fclose(fp);
}

//*************************************************************************************
//*************************************************************************************

void trazenjeZaposlenika()
{
	int pronadeno = 0;
	int idZaposlenika = 0;
	ZAPOSLENIKiNFO citanjeTrazenogZaposlenika = { 0 };
	FILE* fp = NULL;
	fp = fopen(NAZIV_DATOTEKE, "rb");
	if (fp == NULL)
	{
		printf("\n\t\t\tDatoteka nije otvorena!\n");
		exit(1);
	}
	nazivOdabranogIzbornika("PRETRAGA ZAPOSLENIKA");

	if (fseek(fp, VELICINA_MEM_PRIJAVA, SEEK_SET) != 0)            //fseek sluzi za postavljanje pozicije u bin datoteci (u ovom slucaju se postavlja na mjesto nakon velicine memorije strukture podataka za prijavu)
	{
		fclose(fp);
		printf("\n\t\t\tGreska prilikom otvaranja datoteke!\n");
		exit(1);
	}
	printf("\n\n\t\t\tUnesite ID trazenog zaposlenika:");
	fflush(stdin);
	scanf("%u", &idZaposlenika);
	while (fread(&citanjeTrazenogZaposlenika, sizeof(citanjeTrazenogZaposlenika), 1, fp))  //izvrsava se citanje za odabrani id korisnika
	{
		if (citanjeTrazenogZaposlenika.idZaposlenika == idZaposlenika)
		{
			pronadeno = 1;
			break;
		}
	}
	if (pronadeno)
	{
		printf("\n\t\t\tZaposlenikov ID = %d\n", citanjeTrazenogZaposlenika.idZaposlenika);
		printf("\n\t\t\tIme zaposlenika = %s", citanjeTrazenogZaposlenika.imeZaposlenika);
		printf("\n\t\t\tPrezime zaposlenika = %s", citanjeTrazenogZaposlenika.prezime);
		printf("\t\t\tPlaca zaposlenika= %.2f kn\n", citanjeTrazenogZaposlenika.placaZaposlenika);
		printf("\t\t\tIme oca = %s", citanjeTrazenogZaposlenika.imeOca);
		printf("\n\t\t\tAdresa zaposlenika = %s", citanjeTrazenogZaposlenika.adresaZaposlenika);
		printf("\t\t\tDatum zaposljavanja(dd.mm.gggg) =  (%d.%d.%d)", citanjeTrazenogZaposlenika.datumZaposljavanja.dd,
			citanjeTrazenogZaposlenika.datumZaposljavanja.mm, citanjeTrazenogZaposlenika.datumZaposljavanja.gggg);
	}
	else
	{
		printf("\n\t\t\tNije pronadeno!");
	}
	fclose(fp);
	printf("\n\n\n\t\t\tPritisnite tipku za povratak u izbornik...");
	getchar();
}


//*************************************************************************************
//*************************************************************************************
int provjeraBrojaZaposlenih()
{
	ZAPOSLENIKiNFO zaposleniciCitanje = { 0 };
	FILE* fp = NULL;
	int brojZaposlenika = 0;
	fp = fopen(NAZIV_DATOTEKE, "rb");
	if (fp == NULL)
	{
		printf("\n\t\t\tNije moguce otvoriti datoteku!");
		exit(1);
	}
	if (fseek(fp, VELICINA_MEM_PRIJAVA, SEEK_SET) != 0)
	{
		fclose(fp);

		exit(1);
	}
	rewind(fp);                            //sluzi za vracanje pozicije na pocetak datoteke
	fseek(fp, VELICINA_MEM_PRIJAVA, SEEK_SET);
	while (fread(&zaposleniciCitanje, sizeof(ZAPOSLENIKiNFO), 1, fp))
	{
		brojZaposlenika++;
	}
	fclose(fp);


	return brojZaposlenika;
}

//*************************************************************************************
//*************************************************************************************

int pregledSvihZaposlenika()
{
	int pronadeno = 0;
	unsigned int brojZaposlenika = 0;
	ZAPOSLENIKiNFO zaposleniciCitanje = { 0 };
	FILE* fp = NULL;
	nazivOdabranogIzbornika("PREGLED SVIH ZAPOSLENIKA");
	fp = fopen(NAZIV_DATOTEKE, "rb");
	if (fp == NULL)
	{
		printf("Datoteka nije otvorena!\n");
		exit(1);
	}
	if (fseek(fp, VELICINA_MEM_PRIJAVA, SEEK_SET) != 0)  //zato sto fseek() vraca 0 ako je pomak bio uspijesan
	{
		fclose(fp);
		printf("Greska u citanju!\n");
		exit(1);
	}

	while (fread(&zaposleniciCitanje, sizeof(zaposleniciCitanje), 1, fp))  //vraca integral vrstu podtka dok je moguce citati iz datoteke, odnonsno dok ne dodje do kraja
	{
		printf("\n\n\t\t\tZaposlenikov ID = %u\n", zaposleniciCitanje.idZaposlenika);
		printf("\t\t\tIme zaposlenika = %s", zaposleniciCitanje.imeZaposlenika);
		printf("\t\t\tPrezime zaposlenika = %s", zaposleniciCitanje.prezime);
		printf("\t\t\tPlaca Zaposlenika = %.2f kn\n", zaposleniciCitanje.placaZaposlenika);
		printf("\t\t\tIme oca = %s", zaposleniciCitanje.imeOca);
		printf("\t\t\tAdresa zaposlenika = %s", zaposleniciCitanje.adresaZaposlenika);
		printf("\t\t\tDatum zaposljavanja(dd.mm.gggg) =  (%d.%d.%d)\n\n", zaposleniciCitanje.datumZaposljavanja.dd,
			zaposleniciCitanje.datumZaposljavanja.mm, zaposleniciCitanje.datumZaposljavanja.gggg);
		pronadeno = 1;
		brojZaposlenika++;
	}
	printf("\n\t\t\tBroj zaposlenika = %d\n\n", brojZaposlenika);

	if (pronadeno == 0)
	{
		printf("\n\t\t\tNema zapisa!");
	}
	printf("\n\n\t\t\tPritisnite tipku za povratak u izbornik...");
	fflush(stdin);
	fclose(fp);

}

//*************************************************************************************
//*************************************************************************************



void sortirajPodatke_manje_vece(ZAPOSLENIKiNFO* zaposlenici, int n)
{

	ZAPOSLENIKiNFO temp;              //privremena struktura koja sluzi kako bi se mogao promijeniti indeks dvije strukture u polju
	int i = 0;
	int j = 0;

	for (i = 1; i < n; i++)
	{
		for (j = 0; j < n - i; j++)
		{
			if (zaposlenici[j].placaZaposlenika > zaposlenici[j + 1].placaZaposlenika)
			{
				temp = zaposlenici[j];
				zaposlenici[j] = zaposlenici[j + 1];
				zaposlenici[j + 1] = temp;
			}
		}
	}
}

//*************************************************************************************
//*************************************************************************************

void sortirajPodatke_vece_manje(ZAPOSLENIKiNFO* zaposlenici, int n)
{

	ZAPOSLENIKiNFO temp;
	int i = 0;
	int j = 0;

	for (i = 1; i < n; i++)
	{
		for (j = 0; j < n - i; j++)
		{
			if (zaposlenici[j].placaZaposlenika < zaposlenici[j + 1].placaZaposlenika)
			{
				temp = zaposlenici[j];
				zaposlenici[j] = zaposlenici[j + 1];
				zaposlenici[j + 1] = temp;
			}
		}
	}
}
//*************************************************************************************
//*************************************************************************************

void poredajZaposlenePoPlaci(int brojZaposlenika)
{
	nazivOdabranogIzbornika("LISTA ZAPOSLENIH PREMA PLACI");
	FILE* fp = NULL;
	ZAPOSLENIKiNFO* poredajZaposlene = NULL;
	poredajZaposlene = (ZAPOSLENIKiNFO*)calloc(brojZaposlenika, sizeof(ZAPOSLENIKiNFO));
	int izbor = 0;
	if (poredajZaposlene == NULL)
	{
		printf("Error");
		exit(EXIT_FAILURE);
	}
	fp = fopen(NAZIV_DATOTEKE, "rb");
	if (fp == NULL)
	{
		printf("Datoteka nije otvorena!\n");
		exit(EXIT_FAILURE);
	}

	else {
		fseek(fp, VELICINA_MEM_PRIJAVA, SEEK_SET);
		fread(poredajZaposlene, sizeof(ZAPOSLENIKiNFO), brojZaposlenika, fp);
	}
	if (brojZaposlenika != 0) {
		printf("\n\n\n\n\t\t\tOdaberite opciju za sortiranje:");
		printf("\n\t\t\t****************************************************************************");

		printf("\n\t\t\t1.Od najvece prema najmanjoj placi");
		printf("\n\t\t\t2.Od najmanje prema najvecoj placi");
		printf("\n\t\t\t****************************************************************************");
		do {
			printf("\n\n\t\t\tUnesite broj ispred zeljene opcije:");
			scanf("%d", &izbor);
		} while (izbor < 1 || izbor>2);

		if (izbor == 1)
		{
			sortirajPodatke_vece_manje(poredajZaposlene, brojZaposlenika);
		}
		else if (izbor == 2)
		{
			sortirajPodatke_manje_vece(poredajZaposlene, brojZaposlenika);
		}

		printf("\n\n\n\t\t\tBroj zaposlenih: %d", brojZaposlenika);
		for (int i = 0; i < brojZaposlenika; i++)
		{
			printf("\n\n\t\t\tZaposlenikov ID = %u\n", (poredajZaposlene + i)->idZaposlenika);
			printf("\t\t\tIme zaposlenika = %s", (poredajZaposlene + i)->imeZaposlenika);
			printf("\t\t\tPrezime zaposlenika = %s", (poredajZaposlene + i)->prezime);
			printf("\t\t\tPlaca Zaposlenika = %.2f kn\n", (poredajZaposlene + i)->placaZaposlenika);
			printf("\t\t\tIme oca = %s", (poredajZaposlene + i)->imeOca);
			printf("\t\t\tAdresa zaposlenika = %s", (poredajZaposlene + i)->adresaZaposlenika);
			printf("\t\t\tDatum zaposljavanja(dd.mm.gggg) =  (%d.%d.%d)\n\n", (poredajZaposlene + i)->datumZaposljavanja.dd,
				(poredajZaposlene + i)->datumZaposljavanja.mm, (poredajZaposlene + i)->datumZaposljavanja.gggg);
		}
	}
	else if (brojZaposlenika == 0)
	{
		printf("\n\n\t\t\tNema zapisa!");
	}
	free(poredajZaposlene);
	fclose(fp);

}



//*************************************************************************************
//*************************************************************************************

void brisanjeZaposlenika()
{

	int pronadeno = 0;
	int idZaBrisanje = 0;
	PODATCIzaPRIJAVU datPrijava = { 0 };
	ZAPOSLENIKiNFO dodavanjeZaposlenikaUbazu = { 0 };
	FILE* fp = NULL;
	FILE* privremenaDat = NULL;
	nazivOdabranogIzbornika("BRISANJE ZAPOSLENIKA");
	fp = fopen(NAZIV_DATOTEKE, "rb");
	if (fp == NULL)
	{
		printf("Datoteka nije pronadjena\n");
		exit(1);
	}
	privremenaDat = fopen("privremenaDat.bin", "wb");
	if (privremenaDat == NULL)
	{
		fclose(fp);
		printf("Datoteka nije pronadena\n");
		exit(1);
	}
	fread(&datPrijava, VELICINA_MEM_PRIJAVA, 1, fp);
	fwrite(&datPrijava, VELICINA_MEM_PRIJAVA, 1, privremenaDat);   //prepisuje podtke za prijavu u novu, privremenu datoteku
	printf("\n\t\t\tUnesite ID zaposlenika kojeg zelite obrisati:");
	scanf("%d", &idZaBrisanje);
	while (fread(&dodavanjeZaposlenikaUbazu, sizeof(dodavanjeZaposlenikaUbazu), 1, fp))  //cita podtake svih zaposlenika iz datoteke
	{
		if (dodavanjeZaposlenikaUbazu.idZaposlenika != idZaBrisanje)  // Ako ako id trazenog zaposlenika za brisanje nije jednak trenutnom id zaposlenika on ce se prepisati u  novu dat
		{
			fwrite(&dodavanjeZaposlenikaUbazu, sizeof(dodavanjeZaposlenikaUbazu), 1, privremenaDat);
		}
		else
		{
			pronadeno = 1;
		}
	}

	fclose(fp);
	fclose(privremenaDat);

	remove(NAZIV_DATOTEKE); //uklanjanje stare dat
	rename("privremenaDat.bin", NAZIV_DATOTEKE); //mijenjanje imena privremene dat u dat koja se koristi u ostatku programa
	(pronadeno) ? printf("\n\t\t\tUspijesno obrisano.....") : printf("\n\t\t\tZaposlenik nije pronaden");

}

//*************************************************************************************
//*************************************************************************************

void promjenaPodZaPrijavu(void)
{
	PODATCIzaPRIJAVU datPrijava = { 0 };
	FILE* fp = NULL;
	unsigned char korIme[MAX_KORISNICKO_IME] = { 0 };
	unsigned char lozinka[MAX_LOZINKA] = { 0 };
	unsigned char ocisti[3] = { 0 };                        //Sluzi za ciscenje buffera (zbog nepoznatog razloga enter \n je propadao u korisnicko ime)
	nazivOdabranogIzbornika("Promjena podataka za prijavu");
	fp = fopen(NAZIV_DATOTEKE, "rb+");
	if (fp == NULL)
	{
		printf("Datoteka nije otvorena\n");
		exit(1);
	}
	fread(&datPrijava, VELICINA_MEM_PRIJAVA, 1, fp);
	if (fseek(fp, 0, SEEK_SET) != 0)  //postavljanje pozicije na pocetak datoteke, ako nije moguce dat ce se zatvoriti
	{
		fclose(fp);
		printf("\n\t\t\tGreska prilkom dohvacanja podataka!\n");
		exit(1);
	}
	fgets(ocisti, 3, stdin);
	printf("\n\n\t\t\tNovo korisnicko ime:");
	fflush(stdin);
	fgets(korIme, MAX_KORISNICKO_IME, stdin);
	printf("\n\n\t\t\tNova lozinka:");
	fflush(stdin);
	fgets(lozinka, MAX_LOZINKA, stdin);
	strncpy(datPrijava.korisnickoIme, korIme, sizeof(korIme));
	strncpy(datPrijava.lozinka, lozinka, sizeof(lozinka));
	fwrite(&datPrijava, VELICINA_MEM_PRIJAVA, 1, fp);
	fclose(fp);
	printf("\n\t\t\tPodatci uspijesno promijenjeni!\n");
	fflush(stdin);
	getchar();
	fclose(fp);
	
}

//*************************************************************************************
//*************************************************************************************

void prijava()
{
	unsigned char korisnickoIme[MAX_KORISNICKO_IME] = { 0 };
	unsigned char lozinka[MAX_LOZINKA] = { 0 };
	int L = 0;
	PODATCIzaPRIJAVU datPrijava = { 0 };
	FILE* fp = NULL;
	nazivOdabranogIzbornika("Prijavi se");
	fp = fopen(NAZIV_DATOTEKE, "rb");
	if (fp == NULL)
	{
		printf("Datoteka nije otvorena\n");
		exit(1);
	}
	fread(&datPrijava, VELICINA_MEM_PRIJAVA, 1, fp);
	fclose(fp);
	do
	{
		printf("\n\n\n\t\t\t\tKorisnicko ime:");
		fgets(korisnickoIme, MAX_KORISNICKO_IME, stdin);
		printf("\n\t\t\t\tLozinka:");
		fgets(lozinka, MAX_LOZINKA, stdin);
		if ((!strcmp(korisnickoIme, datPrijava.korisnickoIme)) && (!strcmp(lozinka, datPrijava.lozinka))) //usporedjuje unesne podatke s podatcima u memoriji, strcmp vraca nulu ako su stringovi jendaki
		{
			izbornik();
		}

		else
		{
			printf("\t\t\t\tNeuspjela prijava....Pokusaj ponovno!\n\n");
			L++;
		}
	} while (L <= 3);
	if (L > 3)                          //ako su podatci netocni tri puta za redom program se zatvara
	{
		nazivOdabranogIzbornika("Neuspijela prijava");
		printf("\t\t\t\tKorisnik nije pronaden.");
		getch();
		system("cls");
	}
	fclose(fp);
}

//*************************************************************************************
//*************************************************************************************



void defaultPrijava()        // podatci za prijacu koji sluze pri prvom pokretanju programa ili dok se ne postave novi
{

	const char defaultKorIme[] = "admin\n";
	const char defaultLozinka[] = "admin\n";
	PODATCIzaPRIJAVU datPrijava = { 0 };
	FILE* fp = fopen(NAZIV_DATOTEKE, "rb");
	int status = 0;
	if (fp != NULL)
	{
		status = 1;

	}
	if (!status)
	{
		
		fp = fopen(NAZIV_DATOTEKE, "wb");
		if (fp != NULL)                           //ako se datoteka otvara prvi put spremaju se default podatci
		{
			
			strncpy(datPrijava.lozinka, defaultLozinka, sizeof(defaultLozinka));
			strncpy(datPrijava.korisnickoIme, defaultKorIme, sizeof(defaultKorIme));
			fwrite(&datPrijava, VELICINA_MEM_PRIJAVA, 1, fp);
			fclose(fp);
		}
	}
	fclose(fp);

}

//*************************************************************************************
//*************************************************************************************

void izbornik()
{
	int brojZaposlenika;
	int izbor = 0;
	do
	{
		nazivOdabranogIzbornika("IZBORNIK");
		printf("\n\n\n\t\t\t1.Dodavanje zaposlenika");
		printf("\n\t\t\t2.Pretraga zaposlenika");
		printf("\n\t\t\t3.Pregled svih zaposlenika");
		printf("\n\t\t\t4.Brisanje zaposlenika");
		printf("\n\t\t\t5.Poredaj zaposlene po placi");
		printf("\n\t\t\t6.Promjena podataka za prijavu");
		printf("\n\t\t\t0.Exit");
		printf("\n\n\n\t\t\tUnesite broj ispred zeljene opcije => ");
		scanf("%d", &izbor);
		switch (izbor)
		{
		case 1:
			dodajZaposlenikaUbazu();

			break;
		case 2:
			trazenjeZaposlenika();
			getch();

			break;
		case 3:
			pregledSvihZaposlenika(provjeraBrojaZaposlenih);
			getch();

			break;
		case 4:
			brisanjeZaposlenika();
			getch();

			break;
		case 5:
			brojZaposlenika = provjeraBrojaZaposlenih();
			poredajZaposlenePoPlaci(brojZaposlenika);
			getch();


			break;
		case 6:
			fflush(stdin);
			promjenaPodZaPrijavu();
			getch();
			break;

		case 0:
			printf("\n\n\n\t\t\t\tIzlazak iz programa!!\n\n\n\n\n");
			getch();

			exit(1);
			break;
		default:
			printf("\n\n\n\t\t\tUnos nije prepoznat...Pokusaj ponovno!");
			getch();
		}
	} while (izbor != 0);
}

//*************************************************************************************
//*************************************************************************************