#ifndef head_h
#define head_h


#define MAX_GOD  2020
#define MIN_GOD  1940
#define MAX_KORISNICKO_IME 20
#define MAX_LOZINKA  20
#define NAZIV_DATOTEKE  "bazaZaposlenika.bin"
#define MAX_IME_OCA 15
#define MAX_IME_ZAPOSLENIKA 15
#define MAX_PREZIME 20
#define MAX_ADRESA_ZAPOSLENIKA 150
#define VELICINA_MEM_PRIJAVA  sizeof(PODATCIzaPRIJAVU)



typedef struct
{
	int gggg;
	int mm;
	int dd;
} DATUM;


typedef struct
{
	char korisnickoIme[MAX_KORISNICKO_IME];
	char lozinka[MAX_LOZINKA];
} PODATCIzaPRIJAVU;



typedef struct
{
	char imeOca[MAX_IME_OCA];
	char imeZaposlenika[MAX_IME_ZAPOSLENIKA];
	char prezime[MAX_PREZIME];
	char adresaZaposlenika[MAX_ADRESA_ZAPOSLENIKA];
	DATUM datumZaposljavanja;
	unsigned int idZaposlenika;
	float placaZaposlenika;
} ZAPOSLENIKiNFO;

void izbornik();
void defaultPrijava();
void prijava();
void promjenaPodZaPrijavu(void);
void brisanjeZaposlenika();
void poredajZaposlenePoPlaci(int brojZaposlenika);
void sortirajPodatke_vece_manje(ZAPOSLENIKiNFO* zaposlenici, int n);
void sortirajPodatke_manje_vece(ZAPOSLENIKiNFO* zaposlenici, int n);
int pregledSvihZaposlenika();
int provjeraBrojaZaposlenih();
void trazenjeZaposlenika();
void dodajZaposlenikaUbazu();
void nazivOdabranogIzbornika(const char* tekst);
void ulaznaPoruka();
int provjeraImena(const char* ime);
int provjeraAdrese(const char* adresa);
int provjeriDatum(DATUM* provjeraDatuma);


#endif // head_h



